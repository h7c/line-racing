package main

import (
	"fmt"
	"math/rand"
	"os"
	"time"
)

type player struct {
	head         position
	tail         position
	previousMove string
}

type position struct {
	x, y int
}

const (
	minX = 0
	maxX = 29
	minY = 0
	maxY = 19
)

const (
	LEFT = iota
	RIGHT
	UP
	DOWN
)

var movesList = []string{
	LEFT:  "LEFT",
	RIGHT: "RIGHT",
	UP:    "UP",
	DOWN:  "DOWN",
}

var myself player
var nextMove string
var area [30][20]int

func main() {
	var others [4]player

	nextMove = getRandomMove()
	for {
		// numberOfPlayers: total number of players (2 to 4).
		// myID: your player number (0 to 3).
		var numberOfPlayers, myID int
		fmt.Scan(&numberOfPlayers, &myID)
		start := time.Now()

		for currentPlayerID := 0; currentPlayerID < numberOfPlayers; currentPlayerID++ {
			// X0: starting X coordinate of lightcycle (or -1)
			// Y0: starting Y coordinate of lightcycle (or -1)
			// X1: starting X coordinate of lightcycle (can be the same as X0 if you play before this player)
			// Y1: starting Y coordinate of lightcycle (can be the same as Y0 if you play before this player)
			var X0, Y0, X1, Y1 int
			fmt.Scan(&X0, &Y0, &X1, &Y1)

			// update the area map
			area[X1][Y1] = currentPlayerID + 1

			if currentPlayerID == myID {
				// get the head and tail positions
				myself.head = position{
					x: X1,
					y: Y1,
				}
				myself.tail = position{
					x: X0,
					y: Y0,
				}
				continue
			}

			others[currentPlayerID].head.x = X1
			others[currentPlayerID].head.y = Y1
			others[currentPlayerID].tail.x = X0
			others[currentPlayerID].tail.y = Y0
		}

		for {
			u, d, r, l := getDistances()
			log(fmt.Sprintf("u: %d d: %d r: %d l: %d", u, d, r, l))

			nextMove = getOptimalMove()
			// avoid borders
			if myself.head.x == minX && nextMove == movesList[LEFT] {
				// nextMove = getOptimalMove()
				continue
			}
			if myself.head.x == maxX && nextMove == movesList[RIGHT] {
				// nextMove = getOptimalMove()
				continue
			}
			if myself.head.y == minY && nextMove == movesList[UP] {
				// nextMove = getOptimalMove()
				continue
			}
			if myself.head.y == maxY && nextMove == movesList[DOWN] {
				// nextMove = getOptimalMove()
				continue
			}

			// avoid going on a already-used path
			// this first calculation allows us to avoid calculating out of range
			// values
			max1, max2 := 1, 2
			min1, min2 := -1, -2
			if myself.head.x+max2 > maxX || area[myself.head.x+max2][myself.head.y] != 0 {
				max2 = max1
			}
			if myself.head.x+min2 < maxX || area[myself.head.x+min2][myself.head.y] != 0 {
				min2 = min1
			}
			if myself.head.y+max2 > maxY || area[myself.head.x][myself.head.y+max2] != 0 {
				max2 = max1
			}
			if myself.head.y+min2 < minY || area[myself.head.x][myself.head.y+min2] != 0 {
				min2 = min1
			}

			switch nextMove {
			case movesList[LEFT]:
				if area[myself.head.x+min1][myself.head.y] != 0 || area[myself.head.x+min2][myself.head.y] != 0 {
					// nextMove = getOptimalMove()
					continue
				}
			case movesList[RIGHT]:
				if area[myself.head.x+max1][myself.head.y] != 0 || area[myself.head.x+max2][myself.head.y] != 0 {
					// nextMove = getOptimalMove()
					continue
				}
			case movesList[UP]:
				if area[myself.head.x][myself.head.y+min1] != 0 || area[myself.head.x][myself.head.y+min2] != 0 {
					// nextMove = getOptimalMove()
					continue
				}
			case movesList[DOWN]:
				if area[myself.head.x][myself.head.y+max1] != 0 || area[myself.head.x][myself.head.y+max2] != 0 {
					// nextMove = getOptimalMove()
					continue
				}
			}
			break
		}

		move()
		dumpArea(area)
		dumpStats(myID+1, start)
		myself.previousMove = nextMove
	}
}

func getRandomMove() string {
	s := rand.NewSource(time.Now().UnixNano())
	r := rand.New(s)
	return movesList[r.Intn(4)]
}

func getOptimalMove() string {
	u, d, r, l := getDistances()
	if u > d && u > r && u > l {
		return movesList[UP]
	}

	if d > u && d > r && d > l {
		return movesList[DOWN]
	}

	if r > u && r > d && r > l {
		return movesList[RIGHT]
	}

	if l > u && l > d && l > r {
		return movesList[LEFT]
	}

	return getRandomMove()
}

func getDistances() (int, int, int, int) {
	var u, d, r, l int

	for {
		u++
		if myself.head.y-u < minY || area[myself.head.x][myself.head.y-u] != 0 {
			u--
			break
		}
	}
	for {
		d++
		// if myself.head.y+d > maxY {
		if myself.head.y+d > maxY || area[myself.head.x][myself.head.y+d] != 0 {
			d--
			break
		}
	}
	for {
		r++
		// if myself.head.x+r > maxX {
		if myself.head.x+r > maxX || area[myself.head.x+r][myself.head.y] != 0 {
			r--
			break
		}
	}
	for {
		l++
		// if myself.head.x-l < minX {
		if myself.head.x-l < minX || area[myself.head.x-l][myself.head.y] != 0 {
			l--
			break
		}
	}

	return u, d, r, l
}

func dumpArea(area [30][20]int) {
	for y := minY; y <= maxY; y++ {
		for x := minX; x <= maxX; x++ {
			if area[x][y] != 0 {
				fmt.Fprintf(os.Stderr, "%d", area[x][y])
			} else {
				fmt.Fprintf(os.Stderr, " ")
			}
		}
		fmt.Fprintln(os.Stderr)
	}
}

func dumpStats(id int, start time.Time) {
	fmt.Fprintf(os.Stderr, "\nmy id: %d\ncompute time: %s", id, time.Since(start))
}

func log(msg interface{}) {
	fmt.Fprintln(os.Stderr, msg)
}

func move() {
	fmt.Println(nextMove)
}
